public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	
	
	public GameManager() {
		drawPile = new Deck();
		drawPile.shuffle();
		centerCard = drawPile.drawTopCard();
		playerCard = drawPile.drawTopCard();
	}
	
	public String toString() {
		String s = "------------------------------------------------------\n Center Card: " 
		+ centerCard + "\n Player card: " 
		+ playerCard + "\n------------------------------------------------------\n" 
		+ "******************************************************\n";
		return s;
	}
	
	public void dealCard() {
		drawPile.shuffle();
		centerCard = drawPile.drawTopCard();
		playerCard = drawPile.drawTopCard();	
	}
	
	public int getNumberOfCards() {
		int numOfCards = drawPile.length();
		return numOfCards;
	}
	
	public int calculatePoints() {
		String centerCardSuit = centerCard.getSuit();
		int centerCardRank = centerCard.getRank();
		
		String playerCardSuit = playerCard.getSuit();
		int playerCardRank = playerCard.getRank();
		
		
		if(centerCard.equals(playerCard)) {
			return 4;
		}else if(centerCardSuit.equals(playerCardSuit)){
			return 2;
		}else if(centerCardRank == playerCardRank){
			return 2;
		}else{
			return -1;
		}
	}
}