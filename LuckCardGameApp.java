import java.util.Scanner;
class LuckCardGameApp {
	public static void main(String args []) {
		GameManager manager = new GameManager();
		int totalPoints=0;
		
		//Welcome message
		System.out.println("Welcome to a game");
		
		int numOfCards = manager.getNumberOfCards();
		int round = 1;
		while(numOfCards>1 && totalPoints<5) {
			System.out.println("Round " + round);
			System.out.println(manager);
			totalPoints = totalPoints + manager.calculatePoints();
			manager.dealCard();
			numOfCards = manager.getNumberOfCards();
			round++;
		}
		
		if(totalPoints<5) {
			System.out.println("You lose with " + totalPoints + " point(s)");
		}else{
			System.out.println("You win with " + totalPoints + " points");
		}
	}
}